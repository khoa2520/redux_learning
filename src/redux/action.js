export const addTodoAction = {
    type: 'todoList/addTodo',
    payload: {
        id: 3,
        name: 'name3',
        completed: false,
        priority: 'Low'
    }
}

export const addTodo = (data) => {
    return {
        type: 'todoList/addTodo',
        payload: {
            id: data.id,
            name: data.name,
            completed: data.completed,
            priority: data.priority
        }
    }
}
