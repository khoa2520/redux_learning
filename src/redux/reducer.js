const initState = {
    filters: {
        search: '',
        status: 'All',
        priority: []
    },
    todoList: [
        { id: 1, name: 'name1', completed: false, priority: 'Medium' },
        { id: 2, name: 'name2', completed: true, priority: 'High' },
        { id: 3, name: 'name3', completed: false, priority: 'Low' }
    ]
}

const rootReducer = (state = initState, action) => {
    console.log({ state, action });
    switch (action.type) {
        case 'todoList/addTodo': {
            return {
                ...state,
                todoList: [
                    ...state.todoList,
                    action.payload
                ]
            }

        }
        default:
            return state;
    }
}

export default rootReducer
