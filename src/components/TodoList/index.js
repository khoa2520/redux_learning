import { Col, Row, Input, Button, Select, Tag } from 'antd';
import Todo from '../Todo';
import { useDispatch, useSelector } from 'react-redux';
import { addTodo } from '../../redux/action';
import {v4 as uuidv4} from 'uuid'
import { useState } from 'react';
import {todoListSelector} from './../../redux/selectors'
export default function TodoList() {
  const [todoName, setTodoName] = useState()
  const [priority, setPriority] = useState('Medium')

  const todoList = useSelector(todoListSelector)

  const dispatch = useDispatch()
  const handelInputChange = (e) => {
    setTodoName(e.target.value)
  }
  const handelPriority = (value) =>{
    setPriority(value)
    console.log(value)
  }
  const handelAddButtonClick = () => {
    // useDispatch
    dispatch(addTodo({
      id: uuidv4(),
      name: todoName,
      priority: priority,
      completed: false
    }))
    setTodoName('')
    setPriority('Medium')
  }
  return (
    <Row style={{ height: 'calc(100% - 40px)' }}>
      <Col span={24} style={{ height: 'calc(100% - 40px)', overflowY: 'auto' }}>
        {/* <Todo name='Learn React' prioriry='High' />
        <Todo name='Learn Redux' prioriry='Medium' />
        <Todo name='Learn JavaScript' prioriry='Low' /> */}
        {
          todoList.map((value)=>{
             return (
              <Todo key={value.id} name={value.name} prioriry={value.priority} />
             )
          })
        }
      </Col>
      <Col span={24}>
        <Input.Group style={{ display: 'flex' }} compact>
          <Input value={todoName} onChange={handelInputChange}/>
          <Select defaultValue={priority} onChange={handelPriority} >
            <Select.Option value='High' label='High'>
              <Tag color='red'>High</Tag>
            </Select.Option>
            <Select.Option value='Medium' label='Medium'>
              <Tag color='blue'>Medium</Tag>
            </Select.Option>
            <Select.Option value='Low' label='Low'>
              <Tag color='gray'>Low</Tag>
            </Select.Option>
          </Select>
          <Button type='primary' onClick={handelAddButtonClick}>
            Add
          </Button>
        </Input.Group>
      </Col>
    </Row>
  );
}
